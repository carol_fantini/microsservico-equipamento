package br.org.unirio.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Tranca {
	private Integer id;
	private Integer numero;
	private String bicicleta;
	private String localizacao;
	private String anoDeFabricacao;
	private String modelo;
	private String status;

}