package br.org.unirio.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Bicicleta {
	private Integer id;
	private Integer numero;
	private String marca;
	private String modelo;
	private String ano;
	private String status;
	
}