package br.org.unirio.model;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Totem {
	public Integer id;
	public Integer numero;
	public String localizacao;
	public Set<Tranca> trancas;

	public Totem(int id, int numero, String localizacao) {
		super();
		this.id = id;
		this.numero = numero;
		this.localizacao = localizacao;
	}

}