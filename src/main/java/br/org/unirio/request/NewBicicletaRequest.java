package br.org.unirio.request;

public class NewBicicletaRequest {
	public Integer id;
	public Integer numero;
	public String marca;
	public String modelo;
	public String ano;
	public String status;

	public NewBicicletaRequest() {
	}

	public NewBicicletaRequest(Integer id, Integer numero, String marca, String modelo, String ano, String status) {
		this.id = id;
		this.numero = numero;
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.status = status;
	}
	
}