package br.org.unirio.controller;

import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Tranca;
import br.org.unirio.request.NewBicicletaRequest;
import br.org.unirio.service.BicicletaService;
import br.org.unirio.service.TrancaService;
import io.javalin.http.Context;
import io.javalin.http.ForbiddenResponse;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

public class BicicletaController {

	public BicicletaController() {
		// Controller default
	}

	private static Integer getIdBicicletaByCtx(Context ctx) {
		return Integer.parseInt(ctx.pathParam("idBicicleta"));
	}

	private static boolean statusBicicletaIsValid(Context ctx) {
		if ((ctx.queryParam("statusBicicleta") == null) || !ctx.queryParam("statusTranca").equals("DISPONIVEL")
				&& !ctx.queryParam("statusBicicleta").equals("EM_USO") && !ctx.queryParam("statusTranca").equals("NOVA")
				&& !ctx.queryParam("statusBicicleta").equals("APOSENTADA")
				&& !ctx.queryParam("statusBicicleta").equals("REPARO_SOLICITADO")
				&& !ctx.queryParam("statusBicicleta").equals("EM_REPARO")) {
			return false;
		}
		return true;
	}

	@OpenApi(path = "/bicicleta", // only necessary to include when using static method references
			method = HttpMethod.GET, // only necessary to include when using static method references
			summary = "Retorna todas as bicicletas cadastradas", operationId = "getBicicleta", tags = {
					"Bicicleta" }, responses = { @OpenApiResponse(status = "200") })
	public static void getAllBicicletas(Context ctx) {
		ctx.json(BicicletaService.GetAllBicicletas());
	}

	@OpenApi(path = "/bicicleta", method = HttpMethod.POST, summary = "Adiciona um novo cadastro de bicicleta", operationId = "cadastrarBicicleta", tags = {
			"Bicicleta" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NewBicicletaRequest.class) }), responses = {
							@OpenApiResponse(status = "201"), @OpenApiResponse(status = "400") })
	public static void addBicicleta(Context ctx) {
		NewBicicletaRequest bicicleta = ctx.bodyAsClass(NewBicicletaRequest.class);
		BicicletaService.AddBicicleta(bicicleta.marca, bicicleta.modelo, bicicleta.ano, bicicleta.numero,
				bicicleta.status);
		ctx.status(201);
	}

	@OpenApi(path = "/bicicleta/:idBicicleta", summary = "Editar bicicleta", operationId = "updateBicicletaId", method = HttpMethod.PUT, pathParams = {
			@OpenApiParam(name = "bicicletaId", type = Integer.class, description = "Bicicleta Id") }, tags = {
					"Bicicleta" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = NewBicicletaRequest.class) }), responses = {
									@OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
									@OpenApiResponse(status = "404") })
	public static void updateBicicleta(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		if (bicicleta == null) {
			throw new NotFoundResponse("Bicicleta nao encontrada");
		} else {
			BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
					bicicleta.getModelo(), bicicleta.getAno(), bicicleta.getStatus());
		}
	}

	@OpenApi(path = "/bicicleta/:idBicicleta", summary = "Deletar bicicleta", operationId = "deleteBicicletaId", method = HttpMethod.DELETE, pathParams = {
			@OpenApiParam(name = "bicicletaId", type = Integer.class, description = "Bicicleta Id") }, tags = {
					"Tranca" }, responses = { @OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
							@OpenApiResponse(status = "404") })
	public static void deleteBicicleta(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		if (bicicleta == null) {
			throw new NotFoundResponse("Bicicleta nao encontrada");
		} else {
			BicicletaService.DeleteBicicleta(bicicleta.getId());
		}
	}

	@OpenApi(path = "/bicicleta/:idBicicleta", summary = "Busca Bicicleta pelo ID", operationId = "getBicicletaById", method = HttpMethod.GET, tags = {
			"Bicicleta" }, queryParams = {
					@OpenApiParam(name = "bicicletaId", type = Integer.class, description = "ID da Bicicleta"), }, responses = {
							@OpenApiResponse(status = "200"), @OpenApiResponse(status = "400"),
							@OpenApiResponse(status = "404") })
	public static void getBicicletaById(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		if (bicicleta == null) {
			throw new NotFoundResponse("Bicicleta nao encontrada");
		} else {
			ctx.json(bicicleta);
		}

	}

	@OpenApi(path = "/bicicleta/integrarNaRede", summary = "Colocar uma bicicleta de volta na rede de totens", operationId = "integrarNaRede", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "integrarNaRede", type = Integer.class, description = "integrar a bicicleta na rede") }, tags = {
					"Bicicleta" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = Bicicleta.class) }), responses = { @OpenApiResponse(status = "200"),
									@OpenApiResponse(status = "422") })
	public static void integrarNaRede(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));

		if (bicicleta == null || tranca == null) {
			throw new NotFoundResponse("Bicicleta ou tranca nao encontradas");
		} else {

			BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
					bicicleta.getModelo(), bicicleta.getAno(), "DISPONIVEL");
		}
		BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
				bicicleta.getModelo(), bicicleta.getAno(), "OCUPADA");
	}

	@OpenApi(path = "/bicicleta/retirarDaRede", summary = "Retirar bicicleta para reparo ou aposentadoria", operationId = "retirarDaRede", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "RetirarDaRede", type = Integer.class, description = "retirar a bicicleta da rede") }, tags = {
					"Bicicleta" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = Bicicleta.class) }), responses = { @OpenApiResponse(status = "200"),
									@OpenApiResponse(status = "422") })
	public static void retirarDaRede(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));
		if (bicicleta == null || tranca == null) {
			throw new NotFoundResponse("Bicicleta nao encontrada");
		} else {
			if (bicicleta.getStatus().equals("APOSENTADA")) {
				BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
						bicicleta.getModelo(), bicicleta.getAno(), "APOSENTADA");
			} else {
				BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
						bicicleta.getModelo(), bicicleta.getAno(), "REPARO_SOLICITADO");
			}
			TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(),
					tranca.getLocalizacao(), tranca.getAnoDeFabricacao(), tranca.getModelo(), "LIVRE");
		}
	}

	@OpenApi(path = "/bicicleta/status", summary = "Editar status da bicicleta", operationId = "updateStatusBicicleta", method = HttpMethod.PUT, pathParams = {
			@OpenApiParam(name = "bicicletaStatus", type = Integer.class, description = "Bicicleta Status Update") }, tags = {
					"Bicicleta Status" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = NewBicicletaRequest.class) }), responses = {
									@OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
									@OpenApiResponse(status = "404") })
	public static void updateStatusBicicleta(Context ctx) {
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(getIdBicicletaByCtx(ctx));
		String newStatus = ctx.pathParam("statusBicicleta");
		if (bicicleta == null) {
			throw new NotFoundResponse("Bicicleta nao encontrada");
		} else if (statusBicicletaIsValid(ctx)) {
			throw new ForbiddenResponse("Status nao permitido");
		} else {
			BicicletaService.UpdateStatusBicicleta(bicicleta, newStatus);
		}
	}
	
}