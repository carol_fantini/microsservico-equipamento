package br.org.unirio.controller;

import br.org.unirio.model.Totem;
import br.org.unirio.model.Tranca;
import br.org.unirio.request.NewTotemRequest;
import br.org.unirio.service.TotemService;
import br.org.unirio.service.TrancaService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

public class TotemController {

	public TotemController() {
		// Controller default
	}

	public static Integer getIdTotemByCtx(Context ctx) {
		return Integer.parseInt(ctx.pathParam("idTotem"));
	}

	@OpenApi(path = "/totem", method = HttpMethod.GET, summary = "Retorna todos os totens cadastrados", operationId = "getTotens", tags = {
			"Totem" }, responses = { @OpenApiResponse(status = "200") })
	public static void getAllTotens(Context ctx) {
		ctx.json(TotemService.GetAllTotens());
	}

	@OpenApi(path = "/totem", method = HttpMethod.POST, summary = "Adiciona um novo cadastro de totem", operationId = "cadastrarTotem", tags = {
			"Totem" }, requestBody = @OpenApiRequestBody(content = {
					@OpenApiContent(from = NewTotemRequest.class) }), responses = { @OpenApiResponse(status = "201"),
							@OpenApiResponse(status = "400") })
	public static void addTotem(Context ctx) {
		NewTotemRequest totem = ctx.bodyAsClass(NewTotemRequest.class);
		TotemService.AddTotem(totem.id);
	}

	@OpenApi(path = "/totem/:idTotem", summary = "EditarTotem", operationId = "updateTotemId", method = HttpMethod.PUT, pathParams = {
			@OpenApiParam(name = "idTotem", type = Integer.class, description = "Totem Id") }, tags = {
					"Totem" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = NewTotemRequest.class) }), responses = {
									@OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
									@OpenApiResponse(status = "404") })
	public static void updateTotem(Context ctx) {
		Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
		if (totem == null) {
			throw new NotFoundResponse("Totem nao encontrado");
		} else {
			TotemService.UpdateTotem(totem.getId(), totem.getNumero(), totem.getLocalizacao());
		}
	}

	@OpenApi(path = "/totem/:idTotem", summary = "Deleta totem pelo ID", operationId = "deleteTotemById", method = HttpMethod.DELETE, queryParams = {
			@OpenApiParam(name = "totemId", type = Integer.class, description = "ID do Totem") }, tags = {
					"Totem" }, responses = { @OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
							@OpenApiResponse(status = "404") })
	public static void deleteTotem(Context ctx) {
		Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
		if (totem == null) {
			throw new NotFoundResponse("Totem nao encontrado");
		} else {
			TotemService.DeleteTotem(totem.id);
		}
	}

	@OpenApi(path = "/totem/:idTotem/trancas", summary = "Listar trancas de um totem", operationId = "idTotem", method = HttpMethod.GET, pathParams = {
			@OpenApiParam(name = "totemId", type = Integer.class, description = "Totem id") }, tags = {
					"Totem" }, responses = { @OpenApiResponse(status = "200"), @OpenApiResponse(status = "422"),
							@OpenApiResponse(status = "404") })
	public static void listarTrancasDoTotem(Context ctx) {
		Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
		if (totem == null) {
			throw new NotFoundResponse("Totem nao encontrado");
		} else {
			TotemService.getTrancas(totem);
			ctx.json(totem);
		}
	}

	@OpenApi(path = "/totem/:idTotem/trancas", summary = "Vincular tranca a um totem", operationId = "idTotem", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "totemId", type = Integer.class, description = "Totem id") }, tags = {
					"Totem" }, responses = { @OpenApiResponse(status = "200"), @OpenApiResponse(status = "422"),
							@OpenApiResponse(status = "404") })
	public static void vincularTrancaAoTotem(Context ctx) {
		Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));

		if (totem == null || tranca == null) {
			throw new NotFoundResponse("Totem ou tranca nao encontrados");
		} else {
			TotemService.VinculaTrancaAoTotem(totem, tranca);
		}
	}

	@OpenApi(path = "/totem/:idTotem/trancas", summary = "Desvincular tranca a um totem", operationId = "idTotem", method = HttpMethod.DELETE, pathParams = {
			@OpenApiParam(name = "totemId", type = Integer.class, description = "Totem id") }, tags = {
					"Totem" }, responses = { @OpenApiResponse(status = "200"), @OpenApiResponse(status = "422"),
							@OpenApiResponse(status = "404") })
	public static void desvincularTrancaAoTotem(Context ctx) {
		Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));

		if (totem == null || tranca == null) {
			throw new NotFoundResponse("Totem ou tranca nao encontrado");
		} else {
			TotemService.DesvinculaTrancaAoTotem(totem, tranca);
			ctx.json(totem);
		}
	}
	
}