package br.org.unirio.controller;

import br.org.unirio.model.Tranca;
import br.org.unirio.request.NewTrancaRequest;
import br.org.unirio.service.TrancaService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.HttpMethod;
import io.javalin.plugin.openapi.annotations.OpenApi;
import io.javalin.plugin.openapi.annotations.OpenApiContent;
import io.javalin.plugin.openapi.annotations.OpenApiParam;
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody;
import io.javalin.plugin.openapi.annotations.OpenApiResponse;

public class TrancaController {

	public TrancaController() {
		// Controller default
	}

	public static Integer getIdTrancaByCtx(Context ctx) {
		return Integer.parseInt(ctx.pathParam("idTranca"));
	}

	@OpenApi(path = "/tranca", method = HttpMethod.GET, summary = "Retorna todas as trancas cadastradas", operationId = "getTrancas", tags = {
			"Tranca" }, responses = {
					@OpenApiResponse(status = "200", content = { @OpenApiContent(from = Tranca[].class) }) })
	public static void getAllTrancas(Context ctx) {
		ctx.json(TrancaService.GetAllTrancas());
	}

	@OpenApi(path = "/tranca", method = HttpMethod.POST, summary = "Adiciona um novo cadastro de tranca", operationId = "cadastrarTranca", tags = {
			"Tranca" }, requestBody = @OpenApiRequestBody(content = { @OpenApiContent }), responses = {
					@OpenApiResponse(status = "201"), @OpenApiResponse(status = "400") })
	public static void addTranca(Context ctx) {
		NewTrancaRequest tranca = ctx.bodyAsClass(NewTrancaRequest.class);
		TrancaService.AddTranca(tranca.id);
	}

	@OpenApi(path = "/tranca/:idTranca", summary = "EditarTranca", operationId = "updateTrancaId", method = HttpMethod.PUT, pathParams = {
			@OpenApiParam(name = "idTranca", type = Integer.class, description = "Tranca Id") }, tags = {
					"Tranca" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = NewTrancaRequest.class) }), responses = {
									@OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
									@OpenApiResponse(status = "404") })
	public static void updateTranca(Context ctx) {
		Tranca tranca = TrancaService.FindTrancaById(getIdTrancaByCtx(ctx));
		if (tranca == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(),
					tranca.getLocalizacao(), tranca.getAnoDeFabricacao(), tranca.getModelo(), tranca.getStatus());
		}
	}

	@OpenApi(path = "/tranca/:idTranca", summary = "Busca tranca pelo ID", operationId = "getTrancaById", method = HttpMethod.GET, tags = {
			"Tranca" }, queryParams = {
					@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca") }, responses = {
							@OpenApiResponse(status = "200"), @OpenApiResponse(status = "400"),
							@OpenApiResponse(status = "404") })
	public static void getTrancaById(Context ctx) {
		Tranca trancaResposta = null;
		int idTranca = getIdTrancaByCtx(ctx);
		trancaResposta = TrancaService.FindTrancaById(idTranca);

		if (trancaResposta == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			ctx.json(trancaResposta);
		}
	}

	@OpenApi(path = "/tranca/:idTranca/status", summary = "Alterar Status da Tranca", operationId = "alterarStatusTranca", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "trancaId", type = Integer.class, description = "Tranca Id") }, tags = {
					"Tranca" }, responses = { @OpenApiResponse(status = "204"), @OpenApiResponse(status = "404"),
							@OpenApiResponse(status = "422") })
	public static void updateStatusTranca(Context ctx) {
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));
		String newStatus = ctx.pathParam("statusTranca");
		if (tranca == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			TrancaService.UpdateStatusTranca(tranca, newStatus);
		}
	}

	@OpenApi(path = "/tranca/:idTranca", summary = "Deleta tranca pelo ID", operationId = "deleteTrancaById", method = HttpMethod.DELETE, tags = {
			"Tranca" }, queryParams = {
					@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca") }, responses = {
							@OpenApiResponse(status = "204"), @OpenApiResponse(status = "400"),
							@OpenApiResponse(status = "404") })
	public static void deleteTranca(Context ctx) {
		Tranca tranca = TrancaService.FindTrancaById(getIdTrancaByCtx(ctx));
		if (tranca == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			TrancaService.DeleteTranca(tranca.getId());
		}
	}

	@OpenApi(path = "/tranca/integrarNaRede", summary = "Colocar uma tranca nova ou retornando de reparo de volta na rede de totens", operationId = "integrarNaRedeTranca", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "integrarNaRede", type = Integer.class, description = "integrar na rede a Tranca") }, tags = {
					"Tranca" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = br.org.unirio.model.Tranca.class) }), responses = {
									@OpenApiResponse(status = "200"), @OpenApiResponse(status = "422") })
	public static void integrarNaRedeTranca(Context ctx) {
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));
		if (tranca == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(),
					tranca.getLocalizacao(), tranca.getAnoDeFabricacao(), tranca.getModelo(), "LIVRE");
		}
	}

	@OpenApi(path = "/tranca/retirarDaRedeTranca", summary = "Retirar tranca para reparo ou aposentadoria", operationId = "RetirarDaRedeTranca", method = HttpMethod.POST, pathParams = {
			@OpenApiParam(name = "RetirarDaRedeTranca", type = Integer.class, description = "retirar da rede a Tranca") }, tags = {
					"Tranca" }, requestBody = @OpenApiRequestBody(content = {
							@OpenApiContent(from = br.org.unirio.model.Tranca.class) }), responses = {
									@OpenApiResponse(status = "200"), @OpenApiResponse(status = "422") })
	public static void retirarDaRedeTranca(Context ctx) {
		Tranca tranca = TrancaService.FindTrancaById(TrancaController.getIdTrancaByCtx(ctx));
		if (tranca == null) {
			throw new NotFoundResponse("Tranca nao encontrada");
		} else {
			if (tranca.getStatus().equals("APOSENTADA")) {
				TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(),
						tranca.getLocalizacao(), tranca.getAnoDeFabricacao(), tranca.getModelo(), "APOSENTADA");
			} else {
				TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(),
						tranca.getLocalizacao(), tranca.getAnoDeFabricacao(), tranca.getModelo(), "EM_REPARO");
			}
		}
	}
	
}