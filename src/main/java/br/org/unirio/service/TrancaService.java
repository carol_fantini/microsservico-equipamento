package br.org.unirio.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.org.unirio.model.Tranca;
import io.javalin.http.NotFoundResponse;

public class TrancaService {

	public TrancaService() {
	}

	public static Map<Integer, Tranca> trancas = new HashMap<>();
	public static AtomicInteger lastId;

	static {
		trancas.put(1, new Tranca(1, 212, "TR-79", "Rio de Janeiro", "2019", "Kingway", "LIVRE"));
		trancas.put(2, new Tranca(2, 323, "TR-79", "Rio de Janeiro", "2016", "Kingway", "APOSENTADA"));
		trancas.put(3, new Tranca(3, 432, "TR-79", "Rio de Janeiro", "2013", "Kingway", "NOVA"));
		trancas.put(4, new Tranca(4, 786, "TR-79", "Rio de Janeiro", "2017", "Kingway", "OCUPADA"));
		trancas.put(5, new Tranca(5, 233, "TR-79", "Rio de Janeiro", "2012", "Kingway", "NOVA"));
		trancas.put(6, new Tranca(6, 543, "TR-79", "Rio de Janeiro", "2012", "Kingway", "EM_REPARO"));
		trancas.put(7, new Tranca(7, 856, "TR-79", "Rio de Janeiro", "2011", "Kingway", "OCUPADA"));
		trancas.put(8, new Tranca(8, 543, "TR-79", "Rio de Janeiro", "2021", "Kingway", "EM_REPARO"));
		trancas.put(9, new Tranca(9, 645, "TR-79", "Rio de Janeiro", "2011", "Kingway", "LIVRE"));
		trancas.put(10, new Tranca(10, 543, "TR-79", "Rio de Janeiro", "2011", "Kingway", "APOSENTADA"));
		lastId = new AtomicInteger(trancas.size());
	}

	public static Collection<Tranca> GetAllTrancas() {
		return trancas.values();
	}

	public static Tranca FindTrancaById(int idTranca) {
		return trancas.get(idTranca);
	}

	public static void AddTranca(int numero) {
		int id = lastId.incrementAndGet();
		trancas.put(id, new Tranca(id, numero, null, "Disponivel", null, null, null));
	}

	public static void UpdateTranca(Integer trancaId, Integer numero, String bicicleta, String status, String modelo,
			String localizacao, String anoDeFabricacao) {
		trancas.put(trancaId, new Tranca(trancaId, numero, bicicleta, status, modelo, localizacao, anoDeFabricacao));
	}

	public static void DeleteTranca(int idTranca) {
		trancas.remove(idTranca);
	}

	public static String GetStatusTrancaById(int idTranca) {
		Tranca trancaEncontrada = FindTrancaById(idTranca);
		if (trancaEncontrada != null) {
			return trancaEncontrada.getStatus();
		}
		return null;
	}

	public static void UpdateStatusTranca(Tranca tranca, String newStatus) {
		Tranca lock = FindTrancaById(tranca.getId());
		if ((newStatus == null) || !newStatus.equals("LIVRE") && !newStatus.equals("EM_USO")
				&& !newStatus.equals("NOVA") && !newStatus.equals("APOSENTADA")
				&& !newStatus.equals("REPARO_SOLICITADO") && !newStatus.equals("EM_REPARO")) {
			throw new NotFoundResponse("Status de Tranca nao encontrado");
		} else
			lock.setStatus(newStatus);
	}

}