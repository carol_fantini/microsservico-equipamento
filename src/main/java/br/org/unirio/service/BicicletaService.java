package br.org.unirio.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.org.unirio.model.Bicicleta;

public class BicicletaService {
	
	public BicicletaService() {
	}

	public static Map<Integer, Bicicleta> bicicletas = new HashMap<>();
	public static AtomicInteger lastId;

	static {
		bicicletas.put(1, new Bicicleta(1, 23, "Caloi", "TR-79", "2020", "NOVA"));
		bicicletas.put(2, new Bicicleta(2, 32, "Caloi", "TR-79", "2020", "APOSENTADA"));
		bicicletas.put(3, new Bicicleta(3, 56, "Caloi", "TR-79", "2020", "DISPONIVEL"));
		bicicletas.put(4, new Bicicleta(4, 4, "Caloi", "TR-79", "2020", "EM_USO"));
		bicicletas.put(5, new Bicicleta(5, 1, "Caloi", "TR-79", "2020", "NOVA"));
		bicicletas.put(6, new Bicicleta(6, 2, "Caloi", "TR-79", "2020", "NOVA"));
		bicicletas.put(7, new Bicicleta(7, 3, "Caloi", "TR-79", "2020", "REPARO_SOLICITADO"));
		bicicletas.put(8, new Bicicleta(8, 90, "Caloi", "TR-79", "2020", "DISPONIVEL"));
		bicicletas.put(9, new Bicicleta(9, 7, "Caloi", "TR-79", "2020", "NOVA"));
		bicicletas.put(10, new Bicicleta(10, 8, "Caloi", "TR-79", "2020", "EM_REPARO"));
		lastId = new AtomicInteger(bicicletas.size());
	}

	public static Collection<Bicicleta> GetAllBicicletas() {
		return bicicletas.values();
	}

	public static Bicicleta FindBicicletaById(int id) {
		return bicicletas.get(id);
	}

	public static void AddBicicleta(String marca, String modelo, String ano, Integer numero, String status) {
		{
			int id = lastId.incrementAndGet();
			bicicletas.put(id, new Bicicleta(id, numero, marca, modelo, ano, status));
		}
	}

	public static void UpdateBicicleta(Integer id, Integer numero, String marca, String modelo, String ano,
			String status) {
		bicicletas.put(id, new Bicicleta(id, numero, marca, modelo, ano, status));
	}

	public static void DeleteBicicleta(int id) {
		bicicletas.remove(id);
	}

	public static void UpdateStatusBicicleta(Bicicleta bicicleta, String newStatus) {
		Bicicleta bike = FindBicicletaById(bicicleta.getId());
		bike.setStatus(newStatus);
	}

}