package br.org.unirio.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import br.org.unirio.model.Totem;
import br.org.unirio.model.Tranca;

public class TotemService {

	public TotemService() {
	}

	public static Map<Integer, Totem> totens = new HashMap<>();
	public static AtomicInteger lastId;

	static {
		totens.put(1, new Totem(1, 413, "Rio de Janeiro"));
		totens.put(2, new Totem(2, 633, "Rio de Janeiro"));
		totens.put(3, new Totem(3, 153, "Rio de Janeiro"));
		totens.put(4, new Totem(4, 999, "Rio de Janeiro"));
		totens.put(5, new Totem(5, 205, "Rio de Janeiro"));
		totens.put(6, new Totem(6, 303, "Rio de Janeiro"));
		totens.put(7, new Totem(7, 713, "Rio de Janeiro"));
		totens.put(8, new Totem(8, 956, "Rio de Janeiro"));
		totens.put(9, new Totem(9, 127, "Rio de Janeiro"));
		totens.put(10, new Totem(9, 219, "Rio de Janeiro"));
		lastId = new AtomicInteger(totens.size());
	}

	public static Collection<Totem> GetAllTotens() {
		return totens.values();
	}

	public static Totem FindTotemById(int idTotem) {
		return totens.get(idTotem);
	}

	public static void AddTotem(Integer numero) {
		int id = lastId.incrementAndGet();
		totens.put(id, new Totem(id, numero, null));
	}

	public static void UpdateTotem(Integer idTotem, Integer numero, String localizacao) {
		totens.put(idTotem, new Totem(idTotem, numero, localizacao));
	}

	public static void DeleteTotem(Integer idTotem) {
		totens.remove(idTotem);
	}

	public static void getTrancas(Totem totem) {
		totem.getTrancas();
	}

	public static void VinculaTrancaAoTotem(Totem totem, Tranca tranca) {
		totem.trancas.add(tranca);
	}

	public static void DesvinculaTrancaAoTotem(Totem totem, Tranca tranca) {
		totem.trancas.remove(tranca);
	}

}