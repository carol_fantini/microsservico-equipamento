package br.org.unirio.equipamento;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.patch;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.info.Info;

public class JavalinApp {

	  private static Javalin app = Javalin.create(config -> {
		    config.defaultContentType = "application/json";
		  });

	public static void main(String[] args) {
		startApp(getHerokuAssignedPort());
	}

	public static void startApp(int port) {
		app.create().routes(() -> {
			path("tranca", () -> {
				get(br.org.unirio.controller.TrancaController::getAllTrancas);
				post(br.org.unirio.controller.TrancaController::addTranca);
				path(":idTranca", () -> {
					get(br.org.unirio.controller.TrancaController::getTrancaById);
					delete(br.org.unirio.controller.TrancaController::deleteTranca);
					patch(br.org.unirio.controller.TrancaController::updateTranca);
				});
				path(":idTranca/status", () -> {
					post(br.org.unirio.controller.TrancaController::updateStatusTranca);
				});
				path("integrarNaRede", () -> {
					patch(br.org.unirio.controller.TrancaController::integrarNaRedeTranca);
				});
				path("retirarDaRedeTranca", () -> {
					patch(br.org.unirio.controller.TrancaController::retirarDaRedeTranca);
				});
			});

			path("totem", () -> {
				get(br.org.unirio.controller.TotemController::getAllTotens);
				post(br.org.unirio.controller.TotemController::addTotem);
				path(":IdTotem", () -> {
					delete(br.org.unirio.controller.TotemController::deleteTotem);
					patch(br.org.unirio.controller.TotemController::updateTotem);
				});
				path(":idTotem/trancas", () -> {
					get(br.org.unirio.controller.TotemController::listarTrancasDoTotem);
					post(br.org.unirio.controller.TotemController::vincularTrancaAoTotem);
					delete(br.org.unirio.controller.TotemController::desvincularTrancaAoTotem);
				});
			});

			path("bicicleta", () -> {
				get(br.org.unirio.controller.BicicletaController::getAllBicicletas);
				post(br.org.unirio.controller.BicicletaController::addBicicleta);
				path("integrarNaRede", () -> {
					patch(br.org.unirio.controller.BicicletaController::integrarNaRede);
				});
				path("retirarDaRede", () -> {
					patch(br.org.unirio.controller.BicicletaController::retirarDaRede);
				});
				path(":idBicicleta", () -> {
					get(br.org.unirio.controller.BicicletaController::getBicicletaById);
					delete(br.org.unirio.controller.BicicletaController::deleteBicicleta);
					patch(br.org.unirio.controller.BicicletaController::updateBicicleta);
				});
				path(":idBicicleta/status", () -> {
					post(br.org.unirio.controller.BicicletaController::updateStatusBicicleta);
				});
			});

		});
	}

	public void start(int port) {
		this.app.start(port);
	}

	private static OpenApiPlugin getConfiguredOpenApiPlugin() {
		Info info = new Info().version("1.0").description("Equipamento API");
		OpenApiOptions options = new OpenApiOptions(info).activateAnnotationScanningFor("io.javalin.example.java")
				.path("/swagger-docs") // endpoint for OpenAPI json
				.swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
				// .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
				.defaultDocumentation(doc -> {
					doc.json("500", ApiResponse.class);
					doc.json("503", ApiResponse.class);
				});
		return new OpenApiPlugin(options);
	}

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}

	public void stop() {
		app.stop();
	}

}