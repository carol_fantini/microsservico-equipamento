import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import br.org.unirio.controller.BicicletaController;
import br.org.unirio.controller.TotemController;
import br.org.unirio.controller.TrancaController;
import br.org.unirio.model.Bicicleta;
import br.org.unirio.model.Totem;
import br.org.unirio.model.Tranca;
import br.org.unirio.request.NewBicicletaRequest;
import br.org.unirio.request.NewTotemRequest;
import br.org.unirio.request.NewTrancaRequest;
import br.org.unirio.service.BicicletaService;
import br.org.unirio.service.TotemService;
import br.org.unirio.service.TrancaService;

class UnitTest {

	/* Testes dos metodos referentes a TRANCA */

	// getTrancaById
	@Test
	void testGetTrancaByIdValido() {
		Integer idEsperado = 3;
		Integer idResultado = TrancaService.FindTrancaById(idEsperado).getId();
		assertEquals(idEsperado, idResultado);
	}

	// getAllTrancas
	@Test
	void testGetAllTrancas() {
		Collection<Tranca> trancasEsperadas = TrancaService.trancas.values();
		Collection<Tranca> trancasResultado = TrancaService.GetAllTrancas();
		assertEquals(trancasEsperadas, trancasResultado);
	}

	// addTranca
	@Test
	void testAddTranca() {
		int quantidadeTrancasAnterior = TrancaService.trancas.size();
		Integer numeroTrancaNova = 23564;
		Integer numeroTrancaNova2 = 65484;
		Integer numeroTrancaNova3 = 32198;
		TrancaService.AddTranca(numeroTrancaNova);
		TrancaService.AddTranca(numeroTrancaNova2);
		TrancaService.AddTranca(numeroTrancaNova3);
		int quantidadeTrancasAtual = TrancaService.trancas.size();
		assertTrue(quantidadeTrancasAtual > quantidadeTrancasAnterior);
	}

	// updateTranca
	@Test
	void testUpdateTranca() {
		TrancaService.trancas.put(8, new Tranca(8, 543, "TR-79", "Rio de Janeiro", "2021", "Kingway", "EM_REPARO"));
		Tranca tranca = TrancaService.FindTrancaById(8);

		TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(), tranca.getLocalizacao(),
				tranca.getAnoDeFabricacao(), "OnGuard", tranca.getStatus());
		Tranca trancaAtualizada = TrancaService.FindTrancaById(8);
		assertEquals("OnGuard", trancaAtualizada.getModelo());
	}

	// deleteTranca
	@Test
	void testDeleteTranca() {
		int quantidadeTrancas = TrancaService.trancas.size();
		Integer idTrancaDeletada = 2;
		TrancaService.DeleteTranca(idTrancaDeletada);
		assertEquals(TrancaService.trancas.size(), quantidadeTrancas - 1);
		assertNull(TrancaService.FindTrancaById(idTrancaDeletada));
	}

	// updateStatusTranca
	@Test
	void testUpdateStatusTranca() {
		TrancaService.trancas.put(11, new Tranca(6, 543, "TR-79", "Kingway", "Rio de Janeiro", "2011", "LIVRE"));
		Tranca tranca = TrancaService.FindTrancaById(6);
		TrancaService.UpdateStatusTranca(tranca, "EM_REPARO");
		Tranca trancaAtualizada = TrancaService.FindTrancaById(6);
		assertEquals("EM_REPARO", trancaAtualizada.getStatus());
	}

	// integrarNaRedeTranca
	@Test
	void testIntegrarNaRedeTranca() {
		TrancaService.trancas.put(2, new Tranca(2, 323, "TR-79", "Kingway", "2016", "Rio de Janeiro", "EM_REPARO"));
		Tranca tranca = TrancaService.FindTrancaById(2);
		TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(), tranca.getLocalizacao(),
				tranca.getAnoDeFabricacao(), tranca.getModelo(), "LIVRE");
		Tranca trancaAtualizada = TrancaService.FindTrancaById(2);
		assertEquals("LIVRE", trancaAtualizada.getStatus());
	}

	// retirarDaRedeTranca
	@Test
	void testRetirarDaRedeTranca() {
		TrancaService.trancas.put(2, new Tranca(2, 323, "TR-79", "Kingway", "Rio de Janeiro", "2016", "LIVRE"));
		Tranca tranca = TrancaService.FindTrancaById(2);
		TrancaService.UpdateTranca(tranca.getId(), tranca.getNumero(), tranca.getBicicleta(), tranca.getLocalizacao(),
				tranca.getAnoDeFabricacao(), tranca.getModelo(), "APOSENTADA");
		Tranca trancaAtualizada = TrancaService.FindTrancaById(2);
		assertEquals("APOSENTADA", trancaAtualizada.getStatus());
	}

	/* Testes dos metodos referentes a TOTEM */

	// getAllTotens
	@Test
	void testGetAllTotens() {
		Collection<Totem> totensEsperados = TotemService.totens.values();
		Collection<Totem> totensResultado = TotemService.GetAllTotens();
		assertEquals(totensEsperados, totensResultado);
	}

	// addTotem
	@Test
	void testAddTotem() {
		int quantidadeTotens = TotemService.totens.size();
		Integer numeroTotemNovo = 2345678;
		TotemService.AddTotem(numeroTotemNovo);
		assertEquals(TotemService.totens.size(), quantidadeTotens + 1);
	}

	// updateTotem
	@Test
	void testUpdateTotem() {
		TotemService.totens.put(4, new Totem(4, 999, "Rio de Janeiro"));
		Totem totem = TotemService.FindTotemById(4);
		TotemService.UpdateTotem(totem.getId(), totem.getNumero(), "São Paulo");
		Totem totemAtualizada = TotemService.FindTotemById(4);
		assertEquals("São Paulo", totemAtualizada.getLocalizacao());
	}

	// deleteTotem
	@Test
	void testDeleteTotemWithValidId() {
		Integer idDeletado = 2;
		TotemService.DeleteTotem(idDeletado);
		assertNull(TotemService.FindTotemById(idDeletado));
	}

	// listarTrancasDoTotem
	// @Test
	void testListarTrancasDoTotem() {
		TotemService.totens.put(4, new Totem(4, 999, "Rio de Janeiro"));
		Totem totem = TotemService.FindTotemById(4);
		Tranca tranca = new Tranca(1, 212, "TR-79", "Rio de Janeiro", "2019", "Kingway", "LIVRE");
		Set<Tranca> trancas = new HashSet<Tranca>();
		trancas.add(tranca);
		TotemService.VinculaTrancaAoTotem(totem, tranca);
		Totem totemAtualizada = TotemService.FindTotemById(4);
		assertEquals(trancas, totemAtualizada.getTrancas());

	}

	// vincularTrancaAoTotem
	// @Test
	void testVincularTrancaAoTotem() {
		TotemService.totens.put(4, new Totem(4, 999, "Rio de Janeiro"));
		Totem totem = TotemService.FindTotemById(4);
		Tranca tranca = new Tranca(1, 212, "TR-79", "Rio de Janeiro", "2019", "Kingway", "LIVRE");
		Set<Tranca> trancas = new HashSet<Tranca>();
		trancas.add(tranca);
		TotemService.VinculaTrancaAoTotem(totem, tranca);
		Totem totemAtualizada = TotemService.FindTotemById(4);
		assertEquals(trancas, totemAtualizada.getTrancas());
	}

	// desvincularTrancaAoTotem
	// @Test
	void testDesvincularTrancaAoTotem() {
		TotemService.totens.put(4, new Totem(4, 999, "Rio de Janeiro"));
		Totem totem = TotemService.FindTotemById(4);
		Tranca tranca = new Tranca(1, 212, "TR-79", "Rio de Janeiro", "2019", "Kingway", "LIVRE");
		Set<Tranca> trancas = new HashSet<Tranca>();
		trancas.add(tranca);
		trancas.remove(tranca);
		TotemService.VinculaTrancaAoTotem(totem, tranca);
		TotemService.DesvinculaTrancaAoTotem(totem, tranca);
		Totem totemAtualizada = TotemService.FindTotemById(4);
		assertEquals(null, totemAtualizada.getTrancas());
	}

	/* Testes dos metodos referentes a BICICLETA */

	// getAllBicicletas
	@Test
	void testGetAllBicicletas() {
		Collection<Bicicleta> bicicletasEsperadas = BicicletaService.bicicletas.values();
		Collection<Bicicleta> bicicletasResultado = BicicletaService.GetAllBicicletas();
		assertEquals(bicicletasEsperadas, bicicletasResultado);
	}

	// addBicicleta
	@Test
	void testAddBicicleta() {
		int quantidadeBicicletas = BicicletaService.bicicletas.size();
		Integer numeroBicicletaNovo = 234;
		BicicletaService.AddBicicleta("Caloi", "TR-09", "2018", numeroBicicletaNovo, "DISPONIVEL");
		assertEquals(BicicletaService.bicicletas.size(), quantidadeBicicletas + 1);
	}

	// updateBicicleta
	@Test
	void testUpdateBicicleta() {
		BicicletaService.bicicletas.put(8, new Bicicleta(8, 90, "Caloi", "TR-79", "2020", "DISPONIVEL"));
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(8);
		BicicletaService.UpdateBicicleta(bicicleta.getId(), 91, bicicleta.getMarca(), bicicleta.getModelo(),
				bicicleta.getAno(), bicicleta.getStatus());
		Bicicleta bicicletaAtualizada = BicicletaService.FindBicicletaById(8);
		assertEquals(91, bicicletaAtualizada.getNumero());
	}

	// deleteBicicleta
	@Test
	void testBicicletaTotem() {
		int quantidadeBicicletas = BicicletaService.bicicletas.size();
		Integer idBicicletaDeletado = 3;
		BicicletaService.DeleteBicicleta(idBicicletaDeletado);
		assertEquals(BicicletaService.bicicletas.size(), quantidadeBicicletas - 1);
		assertNull(BicicletaService.FindBicicletaById(idBicicletaDeletado));
	}

	// alterarStatusBicicleta
	@Test
	void testupdateStatusBicicleta() {
		BicicletaService.bicicletas.put(8, new Bicicleta(8, 90, "Caloi", "TR-79", "2020", "APOSENTADA"));
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(8);
		BicicletaService.UpdateStatusBicicleta(bicicleta, "EM_REPARO");
		Bicicleta bicicletaAtualizada = BicicletaService.FindBicicletaById(8);
		assertEquals("EM_REPARO", bicicletaAtualizada.getStatus());
	}

	// getBicicletaById
	@Test
	void testGetBicicletaByIdValid() {
		Integer idEsperado = 6;
		Integer idResultado = BicicletaService.FindBicicletaById(idEsperado).getId();
		assertEquals(idEsperado, idResultado);
	}

	// integrarNaRedeBicicleta
	@Test
	void testIntegrarNaRede() {
		BicicletaService.bicicletas.put(8, new Bicicleta(7, 9, "Caloi", "TR-80", "2021", "APOSENTADA"));
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(8);
		BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
				bicicleta.getModelo(), bicicleta.getAno(), "DISPONIVEL");
		Bicicleta bicicletaAtualizada = BicicletaService.FindBicicletaById(7);
		assertEquals("DISPONIVEL", bicicletaAtualizada.getStatus());
	}

	// retirarDaRedeBicicleta
	@Test
	void testRetirarDaRede() {
		BicicletaService.bicicletas.put(8, new Bicicleta(8, 10, "Caloi", "TR-81", "2021", "DISPONIVEL"));
		Bicicleta bicicleta = BicicletaService.FindBicicletaById(8);
		BicicletaService.UpdateBicicleta(bicicleta.getId(), bicicleta.getNumero(), bicicleta.getMarca(),
				bicicleta.getModelo(), bicicleta.getAno(), "EM_REPARO");
		Bicicleta bicicletaAtualizada = BicicletaService.FindBicicletaById(8);
		assertEquals("EM_REPARO", bicicletaAtualizada.getStatus());
	}

	/* Testes dos Construtores */

	// Totem
	@Test
	void testConstrutorTotem() {
		Totem totemTeste = new Totem(6, 10, "Rio de Janeiro");
		Integer totemIdEsperado = 6;
		Integer totemNumeroEsperado = 10;
		String totalLocalizacaoEsperada = "Rio de Janeiro";
		assertEquals(totemIdEsperado, totemTeste.id);
		assertEquals(totemNumeroEsperado, totemTeste.numero);
		assertEquals(totalLocalizacaoEsperada, totemTeste.localizacao);
		assertAll("totemTeste", () -> assertEquals(6, totemTeste.id), () -> assertEquals(10, totemTeste.numero),
				() -> assertEquals("Rio de Janeiro", totemTeste.localizacao));
	}

	@Test
	void testConstrutorTotemService() {
		TotemService totemServiceTeste = new TotemService();
		assertNotNull(totemServiceTeste);
	}

	@Test
	void testConstrutorTotemController() {
		TotemController totemControllerTeste = new TotemController();
		assertNotNull(totemControllerTeste);
	}

	// NewTotemRequest
	@Test
	void testConstrutorNewTotemRequestVazio() {
		NewTotemRequest totemRequestTeste = new NewTotemRequest();
		assertNotNull(totemRequestTeste);
	}

	@Test
	void testConstrutorNewTotemRequest() {
		NewTotemRequest totemRequestTeste = new NewTotemRequest(6);
		Integer totemIdEsperado = 6;
		assertNotNull(totemRequestTeste);
		assertEquals(totemIdEsperado, totemRequestTeste.id);
	}

	// Tranca
	@Test
	void testConstrutorTranca() {
		Tranca trancaTeste = new Tranca(1, 212, "TR-79", "Rio de Janeiro", "2019", "Kingway", "LIVRE");
		Integer trancaIdEsperado = 1;
		Integer trancaNumeroEsperado = 212;
		String statusTrancaEsperado = "LIVRE";
		assertEquals(trancaIdEsperado, trancaTeste.getId());
		assertEquals(trancaNumeroEsperado, trancaTeste.getNumero());
		assertEquals(statusTrancaEsperado, trancaTeste.getStatus());
	}

	@Test
	void testConstrutorTrancaService() {
		TrancaService trancaServiceTeste = new TrancaService();
		assertNotNull(trancaServiceTeste);
	}

	@Test
	void testConstrutorTrancaController() {
		TrancaController trancaControllerTeste = new TrancaController();
		assertNotNull(trancaControllerTeste);
	}

	// NewTrancaRequest
	@Test
	void testConstrutorNewTrancaRequestVazio() {
		NewTrancaRequest trancaRequestTeste = new NewTrancaRequest();
		assertNotNull(trancaRequestTeste);
	}

	@Test
	void testConstrutorNewTrancaRequest() {
		NewTrancaRequest trancaRequestTeste = new NewTrancaRequest(9);
		Integer trancaIdEsperado = 9;
		assertNotNull(trancaRequestTeste);
		assertEquals(trancaIdEsperado, trancaRequestTeste.id);
	}

	// Bicicleta
	@Test
	void testConstrutorBicicleta() {
		Bicicleta bicicletaTest = new Bicicleta(1, 23, "Caloi", "TR-79", "2020", "NOVA");
		Integer bicicletaIdEsperado = 1;
		Integer bicicletaNumeroEsperado = 23;
		String statusBicicletaEsperado = "NOVA";
		assertEquals(bicicletaIdEsperado, bicicletaTest.getId());
		assertEquals(bicicletaNumeroEsperado, bicicletaTest.getNumero());
		assertEquals(statusBicicletaEsperado, bicicletaTest.getStatus());
	}

	@Test
	void testConstrutorBicicletaService() {
		BicicletaService bicicletaServiceTeste = new BicicletaService();
		assertNotNull(bicicletaServiceTeste);
	}

	@Test
	void testConstrutorBicicletaController() {
		BicicletaController bicicletaControllerTest = new BicicletaController();
		assertNotNull(bicicletaControllerTest);
	}

	// NewBicicletaRequest
	@Test
	void testConstrutorNewBicicletaRequestVazio() {
		NewBicicletaRequest newBicicletaRequestTest = new NewBicicletaRequest();
		assertNotNull(newBicicletaRequestTest);
	}

	@Test
	void testConstrutorNewBicicletaRequest() {
		NewBicicletaRequest bicicletaRequestTeste = new NewBicicletaRequest(7, 21, "Caloi", "TR-90", "2021", "NOVA");
		Integer bicicletaNumeroEsperado = 21;
		assertEquals(bicicletaNumeroEsperado, bicicletaRequestTeste.numero);
	}

}